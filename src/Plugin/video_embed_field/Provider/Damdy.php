<?php

declare(strict_types = 1);

namespace Drupal\video_embed_damdy\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Damdy provider plugin.
 *
 * @VideoEmbedProvider(
 *     id = "damdy",
 *     title = @Translation("Damdy")
 * )
 */
class Damdy extends ProviderPluginBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $config = $this->configFactory->getEditable('video_embed_damdy.settings');

    return [
      '#theme' => 'video_embed_damdy',
      '#media_id' => $this->getVideoId(),
      '#publisher_id' => !empty($config->get('damdy_publisher_id')) ? $config->get('damdy_publisher_id') : '',
      '#medias_xml_url' => !empty($config->get('damdy_media_xml_url')) ? $config->get('damdy_media_xml_url') : '',
      '#player_params_url' => !empty($config->get('damdy_param_url')) ? $config->get('damdy_param_url') : '',
      '#player_guid' => !empty($config->get('damdy_guid')) ? $config->get('damdy_guid') : '',
      '#autostart' => ($autoplay) ? '1' : '0',
      '#attached' => [
        'library' => [
          'video_embed_damdy/video-embed-damdy',
        ],
        'drupalSettings' => ['damdy_config_js' => !empty($config->get('damdy_config_js')) ? $config->get('damdy_config_js') : ''],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    $damdyPoster = '';
    $config = $this->configFactory->getEditable('video_embed_damdy.settings');

    $xmlUrl = !empty($config->get('damdy_media_xml_url')) ? $config->get('damdy_media_xml_url') : '';
    if (!empty($xmlUrl)) {
      $xmlUrl .= '&byMediaId=' . $this->getVideoId();
      $infos = \simplexml_load_file($xmlUrl);
      if ($infos && !empty($infos->medias->media) && \count($infos->medias->media->photo_url) > 0) {
        /** @var \SimpleXMLElement $damdyPoster */
        $damdyPoster = $infos->medias->media->photo_url[0];
        $damdyPoster = $damdyPoster->__toString();
      }
    }
    else {
      $damdyPoster = \sprintf('https://www-cdn.wedia-group.com/wp-content/uploads/2018/03/wedia2017blue.png');
    }

    return $damdyPoster;
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    $matches = [];
    \preg_match('/damdy\/(?P<id>\d+)/', $input, $matches);
    return $matches['id'] ?? FALSE;
  }

}
