CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provides a video embed field provider that allows you to embed
videos from Damdy (https://damdy.com).

Users can add Damdy videos to their site by pasting the id of the video prefixed
by 'damdy/' into a video embed field

REQUIREMENTS
------------

This module requires the following modules:
 * Video Embed Field (https://www.drupal.org/project/video_embed_field)

You have to pay to obtain the URLs required in the config form.

INSTALLATION
------------

 * Install and enable this module like any other drupal 8 module.

CONFIGURATION
-------------

 * Enable the Video Embed Damdy module on your main site.
 * Go to the configuration page (admin/config/media/damdy-configuration) and
   choose your settings.
 * On your video embed fields, the Damdy provider will appears in the list of
   allowed providers.

MAINTAINERS
-----------

Current maintainers:
 * Philippe Joulot (phjou) - https://www.drupal.org/user/3344054

This project has been sponsored by:
 * Smile - http://www.smile.fr
